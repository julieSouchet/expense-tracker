const Transaction = require("../models/Transaction");

// @desc    Get all transactions
// @route   GET /api/v1/transactions
// @access  public
exports.getTransactions = async (req, res, next) => {
  try {
    const transactions = await Transaction.find();

    return res.status(200).json({
      success: true,
      count: transactions.length,
      data: transactions,
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      error: "Server error",
    });
  }
};

// @desc    Add all transactions
// @route   POST /api/v1/transactions
// @access  public
exports.addTransactions = async (req, res, next) => {
  try {
    const transaction = await Transaction.create(req.body); // les champs sont automatiquement validés par rapport au modèle

    return res.status(201).json({
      success: true,
      data: transaction,
    });
  } catch (error) {
    if (error.name === "ValidationError") {
      const messages = Object.values(error.errors).map((val) => val.message);

      // = client error
      res.status(400).json({
        success: false,
        errors: messages,
      });
    } else {
      return res.status(500).json({
        success: false,
        error: "Server error",
      });
    }
  }
};

// @desc    Delete all transactions
// @route   DELETE /api/v1/transactions/:id
// @access  public
exports.deleteTransactions = async (req, res, next) => {
  try {
    const transaction = await Transaction.findById(req.params.id);

    if (!transaction) {
      return res.status(404).send({
        success: false,
        error: "Transaction not found",
      });
    } else {
      await transaction.remove();

      return res.status(200).send({
        success: true,
        data: {},
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      error: "Server error",
    });
  }
};
