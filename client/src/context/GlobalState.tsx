import React, { createContext, useReducer } from "react";
import axios, { AxiosError, AxiosResponse } from "axios";
import AppReducer from "./AppReducer";
import { ActionDispatchers, State, Transaction } from "../types";

const initialState: State & ActionDispatchers = {
  transactions: [],
  error: null,
  loading: true,
};

export const GlobalContext = createContext(initialState);

// Provider component so other components have access to the context
export const GlobalProvider = ({
  children,
}: {
  children: JSX.Element[];
}): JSX.Element => {
  // state = the stored data (here, transactions) + the actions functions
  // dispatch is used to call the reducer with a specific action
  const [state, dispatch] = useReducer(AppReducer, initialState);

  // ACTIONS
  const getTransactions = async () => {
    try {
      const res = await axios.get("/api/v1/transactions");

      dispatch({
        type: "GET_TRANSACTIONS",
        payload: res.data.data,
      });
    } catch (error) {
      if (axios.isAxiosError(error)) {
        dispatch({
          type: "TRANSACTION_ERROR",
          payload: error.response.data.error,
        });
      }
    }
  };

  const deleteTransaction = async (id: string) => {
    try {
      const res = await axios.delete("/api/v1/transactions/" + id);

      if (res.data.success) {
        dispatch({
          type: "DELETE",
          payload: id,
        });
      } else {
        dispatch({
          type: "TRANSACTION_ERROR",
          payload: "Invalid transaction ID",
        });
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        dispatch({
          type: "TRANSACTION_ERROR",
          payload: error.response.data.error,
        });
      }
    }
  };

  const addTransaction = async (transaction: Transaction) => {
    try {
      const res = await axios.post("/api/v1/transactions", transaction, {
        headers: { "Content-Type": "application/json" },
      });

      if (res.data.success) {
        dispatch({
          type: "ADD",
          payload: transaction,
        });
      } else {
        dispatch({
          type: "TRANSACTION_ERROR",
          payload: res.data.error,
        });
      }
    } catch (error) {
      if (axios.isAxiosError(error)) {
        dispatch({
          type: "TRANSACTION_ERROR",
          payload: error.response.data.error,
        });
      }
    }
  };

  // provider value props = default value for the context's state
  return (
    <GlobalContext.Provider
      value={{ ...state, deleteTransaction, addTransaction, getTransactions }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
