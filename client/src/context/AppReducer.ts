import { Action, State } from "../types";

// We code the actions' effects (= state update logic) here
export default (state: State, action: Action) => {
  switch (action.type) {
    case "DELETE":
      // removing the transaction whose id is passed in the params
      return {
        ...state,
        transactions: state.transactions.filter(
          (transac) => transac._id !== action.payload
        ),
      };

    case "ADD":
      // adding the transaction passed in the params
      return {
        ...state,
        transactions: [...state.transactions, action.payload],
      };

    case "GET_TRANSACTIONS":
      return {
        ...state,
        transactions: action.payload,
        loading: false,
        error: false,
      };

    case "TRANSACTION_ERROR":
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      console.warn("action not implemented");
      return state;
  }
};
