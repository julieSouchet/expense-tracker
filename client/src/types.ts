export interface Transaction {
  _id: string;
  text: string;
  amount: number;
}

export interface State {
  transactions: Transaction[];
  error: boolean;
  loading: boolean;
}

export interface ActionDispatchers {
  deleteTransaction?: (id: string) => void;
  addTransaction?: (transaction: Transaction) => void;
  getTransactions?: () => Promise<void>;
}

export type Action =
  | { type: "DELETE"; payload: string }
  | { type: "ADD"; payload: Transaction }
  | { type: "TRANSACTION_ERROR"; payload: string }
  | { type: "GET_TRANSACTIONS"; payload: Transaction[] };
