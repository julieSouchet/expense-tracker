import React from "react";
import { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";

function IncomeExpenses() {
  const state = useContext(GlobalContext);

  const income = state.transactions.reduce((sum, transaction) => {
    if (transaction.amount > 0) return sum + transaction.amount;
    else return sum;
  }, 0);

  const expenses = state.transactions.reduce((sum, transaction) => {
    if (transaction.amount < 0) return sum + transaction.amount;
    else return sum;
  }, 0);

  return (
    <div className="inc-exp-container">
      <div>
        <h4>Income</h4>
        <p className="money plus">+${income.toFixed(2)}</p>
      </div>
      <div>
        <h4>Expenses</h4>
        <p className="money minus">-${Math.abs(expenses).toFixed(2)}</p>
      </div>
    </div>
  );
}

export default IncomeExpenses;
