import React from "react";
import { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";

const Balance = () => {
  const state = useContext(GlobalContext);

  const balance = state.transactions.reduce(
    (sum, transaction) => sum + transaction.amount,
    0
  );

  return (
    <>
      <h4>Your Balance</h4>
      <h1>
        {balance > 0 ? "+" : "-"}${Math.abs(balance).toFixed(2)}
      </h1>
    </>
  );
};

export default Balance;
