import React, { useState, useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalState";
import { TransactionItem } from "./TransactionItem";

function TransactionList() {
  const context = useContext(GlobalContext);
  const [showAll, setShowAll] = useState(false);

  useEffect(() => {
    context.getTransactions();
  }, []);

  const transactions = context.transactions
    .map((transaction) => {
      return <TransactionItem {...transaction} key={transaction._id} />;
    })
    .reverse();

  const showAllBtn = (
    <button
      type="button"
      className="btn secondary"
      onClick={() => {
        setShowAll(!showAll);
      }}
    >
      <span
        dangerouslySetInnerHTML={{ __html: showAll ? "&and;" : "&or;" }}
      ></span>
    </button>
  );

  return (
    <>
      <h3>
        History {context.loading ? <span className="loader"></span> : null}
      </h3>
      <ul className="list">
        {showAll ? transactions : transactions.slice(0, 3)}
      </ul>
      {transactions.length > 3 ? showAllBtn : null}
    </>
  );
}

export default TransactionList;
