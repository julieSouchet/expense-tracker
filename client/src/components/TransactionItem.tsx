import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";
import { Transaction } from "../types";

export const TransactionItem = (props: Transaction) => {
  const context = useContext(GlobalContext);

  return (
    <li
      key={"transaction-" + props._id}
      className={props.amount < 0 ? "minus" : "plus"}
    >
      {props.text}
      <span>
        {(props.amount < 0 ? "-" : "+") +
          "$" +
          Math.abs(props.amount).toFixed(2)}
      </span>
      <button
        className="delete-btn"
        onClick={() => context.deleteTransaction(props._id)}
      >
        &times;
      </button>
    </li>
  );
};
