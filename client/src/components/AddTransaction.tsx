import React, { FormEventHandler } from "react";
import { useContext } from "react";
import { ChangeEventHandler } from "react";
import { useState } from "react";
import { GlobalContext } from "../context/GlobalState";

const AddTransaction = () => {
  const context = useContext(GlobalContext);

  const [text, setText] = useState("");
  const [amount, setAmount] = useState(0);

  const textChangeHandler: ChangeEventHandler<HTMLInputElement> = (e) => {
    setText(e.target.value);
  };

  const amountChangeHandler: ChangeEventHandler<HTMLInputElement> = (e) => {
    const value = parseInt(e.target.value);
    if (!isNaN(value)) setAmount(value);
  };

  const submitHandler: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    // e.stopPropagation();

    // get last id then iterate
    const id = context.transactions[context.transactions.length - 1].id + 1;

    context.addTransaction({ id, amount, text });
  };

  return (
    <>
      <h3>Add new transaction</h3>
      <form onSubmit={submitHandler}>
        <div className="form-control">
          <label htmlFor="text">Text</label>
          <input
            type="text"
            value={text}
            onChange={textChangeHandler}
            placeholder="Enter text..."
          />
        </div>
        <div className="form-control">
          <label htmlFor="amount">
            Amount <br />
            (negative = expense, positive = income)
          </label>
          <input
            type="number"
            value={amount}
            onChange={amountChangeHandler}
            placeholder="Enter amount..."
          />
        </div>
        <button className="btn">Add transaction</button>
      </form>
    </>
  );
};

export default AddTransaction;
