const express = require("express");
const router = express.Router();
const {
  getTransactions,
  addTransactions,
  deleteTransactions,
} = require("../controllers/transactions");

// controlleur inclus directement : router.get("/", (req, res) => res.send("Hello transactions"));
// controlleur importé ci-dessous
router
  .route("/") // route (url)

  .get(getTransactions) // méthode get

  .post(addTransactions); // méthode post

router
  .route("/:id") // route avec paramètre

  .delete(deleteTransactions);

module.exports = router;
