const express = require("express");
const dotenv = require("dotenv");
const colors = require("colors");
const morgan = require("morgan");
const connectDB = require("./config/db");

// setup environnement
dotenv.config({ path: "./config/config.env" });
connectDB();

const transactions = require("./routes/transactions");

const app = express();

// ajout d'un middleware pour parser les body de requêtes en json
app.use(express.json());

if (process.env.NODE_ENV !== "production") {
  app.use(morgan("dev"));
}

// monter le router
app.use("/api/v1/transactions", transactions);

// lancer le serveur
const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} on port ${process.env.PORT}`
      .yellow.bold
  )
);
