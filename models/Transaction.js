const mongoose = require("mongoose");

// description de la table et des ses champs
const TransactionSchema = new mongoose.Schema({
  text: {
    type: "string",
    trim: true,
    required: [true, "Text is required"],
  },
  amount: {
    type: "number",
    required: [true, "Amount is required (either positive or negative)"],
  },
  createdAt: {
    type: "date",
    default: Date.now,
  },
});

module.exports = mongoose.model("Transaction", TransactionSchema);
